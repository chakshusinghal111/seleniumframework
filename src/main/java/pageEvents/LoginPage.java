package pageEvents;

import org.testng.Assert;

import pageObjects.LoginPageElements;
import testpackage.BaseTest;
import utils.ElementFetch;

public class LoginPage {

	public void verifyLoginpage()
	{
			ElementFetch elementfetch = new ElementFetch();
			BaseTest.logger.info("verifying login page");
			System.out.println("verifying login page");
			Assert.assertTrue(elementfetch.getListwebelements("XPATH", LoginPageElements.signinText).size() > 0 , " login page did not open " );
			System.out.println("verified login page");
	}
	
	public void enterEmailId()
	{
		ElementFetch elementfetch = new ElementFetch();
		BaseTest.logger.info("verifying email id");
		System.out.println("verifying email id");
		elementfetch.getwebelement("ID", LoginPageElements.emailid).sendKeys("7088168008");
		System.out.println("email id verified");
	}
}
