package testpackage;

import java.io.File;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import pageEvents.HomePageEvents;
import pageEvents.LoginPage;
import utils.Constants;

// org.testng.annotation.AfterMethod;

public class BaseTest {

	public static WebDriver driver;
	public static ExtentHtmlReporter htmlReport;
	public static ExtentReports extentReport;
	public static ExtentTest logger;
	
@BeforeSuite
public void beforeSuitesmethod()
{
	htmlReport = new ExtentHtmlReporter( System.getProperty("user.dir") + File.separator + "report" + File.separator + "AutomationReport.html");
	htmlReport.config().setEncoding("utf-8");
	htmlReport.config().setDocumentTitle("Automation Report");
	htmlReport.config().setReportName("Automation Test Result");
	extentReport = new ExtentReports();
	extentReport.attachReporter(htmlReport);
}


@BeforeMethod
@Parameters(value= {"browserName"})
public void beforemethod(String browserName, Method testmethod)
{
	logger = extentReport.createTest(testmethod.getName());
	setupDriver(browserName);
	driver.manage().window().maximize();
	driver.get(Constants.url);
	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
}

@Test 
public void samplemethodforEmail()
{
	HomePageEvents hp = new HomePageEvents();
	hp.clickonSignIn();
	
	LoginPage lp = new LoginPage();
	lp.verifyLoginpage();
	lp.enterEmailId();
	

}
@AfterMethod
public void aftermethod(ITestResult result)
{
	if ( result.getStatus() == ITestResult.SUCCESS)
	{
		String methodName = result.getMethod().getMethodName();
		String logText = "Test Case : " + methodName + "Passed";
		Markup m = MarkupHelper.createLabel(logText,ExtentColor.GREEN);
		logger.log(Status.PASS,m);
		
	}
	else if ( result.getStatus() == ITestResult.FAILURE)
	{
		String methodName = result.getMethod().getMethodName();
		String logText = "Test Case : " + methodName + "Failed";
		Markup m = MarkupHelper.createLabel(logText,ExtentColor.RED);
		logger.log(Status.FAIL,m);
		
	}
	else if ( result.getStatus() == ITestResult.SKIP)
	{
		String methodName = result.getMethod().getMethodName();
		String logText = "Test Case : " + methodName + "Skipped";
		Markup m = MarkupHelper.createLabel(logText,ExtentColor.YELLOW);
		logger.log(Status.SKIP,m);
		
	}
	
	driver.close();
}

@AfterSuite
public void afterclassmethod()
{
	extentReport.flush();
}

public void setupDriver(String browsername)
{
	
	if (browsername.equalsIgnoreCase("Chrome"))
	{
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + File.separator + "driver" + File.separator + "chromedriver.exe");
		driver = new ChromeDriver();
	}
	else if (browsername.equalsIgnoreCase("firefox"))
	{
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + File.separator + "driver" + File.separator + "geckodriver.exe" );
		driver = new FirefoxDriver();
	}
	else
	{
		System.setProperty("webdriver.Chrome.driver", System.getProperty("user.dir") + File.separator + "driver" + File.separator + "chromedriver.exe" );
		driver = new ChromeDriver();
	}
	
	
}
}
