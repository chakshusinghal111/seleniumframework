package utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import testpackage.BaseTest;

public class ElementFetch {
	
	public WebElement getwebelement(String identifiertype, String identifiervalue) {
		switch(identifiertype) {
		case "ID":
			return BaseTest.driver.findElement(By.id(identifiervalue));
		case "CSS":
			return BaseTest.driver.findElement(By.cssSelector(identifiervalue));
		case "TAGNAME":
			return BaseTest.driver.findElement(By.tagName(identifiervalue));
		case "XPATH":
			return BaseTest.driver.findElement(By.xpath(identifiervalue));
		default:
			return null;
		}
			
	}
	
	public List<WebElement> getListwebelements(String identifiertype, String identifiervalue) {
		switch(identifiertype) {
		case "ID":
			return BaseTest.driver.findElements(By.id(identifiervalue));
		case "CSS":
			return BaseTest.driver.findElements(By.cssSelector(identifiervalue));
		case "TAGNAME":
			return BaseTest.driver.findElements(By.tagName(identifiervalue));

		case "XPATH":
			return BaseTest.driver.findElements(By.xpath(identifiervalue));
		default:
			return null;
		}
			
	}

}
